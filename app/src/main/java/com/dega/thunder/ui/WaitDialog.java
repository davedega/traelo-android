package com.dega.thunder.ui;

import android.app.AlertDialog;
import android.app.Dialog;
import android.graphics.drawable.AnimationDrawable;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.View;
import android.widget.ImageView;

import com.dega.thunder.R;

/**
 * Created by davedega on 31/01/15.
 */
public class WaitDialog extends DialogFragment {

    public static WaitDialog newInstance() {
        WaitDialog frag = new WaitDialog();
        return frag;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        /** construcor con animacion de abajo hacia arriba av traves de estilos **/
        // AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
        // getActivity(), R.style.DialogSlideAnim);
        /** construcor por default **/
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                getActivity());
        View view = getActivity().getLayoutInflater().inflate(
                R.layout.wait_layout, null);
        alertDialogBuilder.setView(view);
        Dialog dia = alertDialogBuilder.create();
//        dia.getWindow().getAttributes().windowAnimations = R.style.dialog_animation;
        ImageView bikeImage = (ImageView) view.findViewById(R.id.bike);
        bikeImage.setBackgroundResource(R.drawable.animatin_bike);

        // Get the AnimationDrawable object.
        AnimationDrawable frameAnimation = (AnimationDrawable) bikeImage.getBackground();

        // Start the animation (looped playback by default).
        frameAnimation.start();

        return dia;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }
}
