package com.dega.thunder.ui;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AutoCompleteTextView;

/**
 * Created by davedega on 07/01/15.
 */
public class ClearableAutoComplete extends AutoCompleteTextView {
    // The image we defined for the clear button
    public Drawable imgClearButton = getResources().getDrawable(
            android.R.drawable.presence_offline);
    // was the text just cleared?
    boolean justCleared = false;
    // if not set otherwise, the default clear listener clears the text in the
    // text view
    private OnClearListener defaultClearListener = new OnClearListener() {

        @Override
        public void onClear() {
            ClearableAutoComplete et = ClearableAutoComplete.this;
            et.setText("");
        }
    };
    private OnClearListener onClearListener = defaultClearListener;

    /* Required methods, not used in this implementation */
    public ClearableAutoComplete(Context context) {
        super(context);
        init();
    }

    /* Required methods, not used in this implementation */
    public ClearableAutoComplete(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
    }

    /* Required methods, not used in this implementation */
    public ClearableAutoComplete(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    void init() {
        // Set the bounds of the button

        this.setCompoundDrawablesWithIntrinsicBounds(null, null,
                imgClearButton, null);
        hideClearButton();

        // if the clear button is pressed, fire up the handler. Otherwise do nothing
        this.setOnTouchListener(new OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {

                ClearableAutoComplete et = ClearableAutoComplete.this;

                if (et.getCompoundDrawables()[2] == null)
                    return false;

                if (event.getAction() != MotionEvent.ACTION_UP)
                    return false;

                if (event.getX() > et.getWidth() - et.getPaddingRight() - imgClearButton.getIntrinsicWidth()) {
                    onClearListener.onClear();
                    justCleared = true;
                }
                return false;
            }
        });
        this.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                Log.i("beforeTextChanged", "count" + count);
                Log.i("beforeTextChanged", "CharSequence" + s.toString());

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                Log.i("onTextChanged", "count" + count);
                Log.i("onTextChanged", "CharSequence" + s.toString());


            }

            @Override
            public void afterTextChanged(Editable s) {
                Log.i("afterTextChanged", "editable: " + s.toString());
                if (s.toString().equals("")) {
                    hideClearButton();
                } else {
                    showClearButton();
                }

            }
        });
    }

    public void setImgClearButton(Drawable imgClearButton) {
        this.imgClearButton = imgClearButton;
    }

    public void setOnClearListener(final OnClearListener clearListener) {
        this.onClearListener = clearListener;
    }

    public void hideClearButton() {
        this.setCompoundDrawables(null, null, null, null);
    }

    public void showClearButton() {
        this.setCompoundDrawablesWithIntrinsicBounds(null, null, imgClearButton, null);
    }

    public interface OnClearListener {
        void onClear();
    }

}