package com.dega.thunder.viewmodel;

/**
 * Created by davedega on 29/01/15.
 */
public interface IWelcomeVMListener {

    public void onViewLoginT();

    public void onViewSignUpT();

    public void onDoLoginT();

    public void onDoSignUpT();
}
