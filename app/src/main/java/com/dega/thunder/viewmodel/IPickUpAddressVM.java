package com.dega.thunder.viewmodel;

/**
 * Created by davedega on 07/01/15.
 */
public interface IPickUpAddressVM {

    public void setListener(IPickUpAddressVMListener listener);

    public void setStreet(String street);

    public String getCalle();

    public String getNumero();

    public String getReferencia();

    public String getNombrePersonaOComercio();

    public String getDescripcionPaquete();

    public boolean getTipoServicio();

    void setViewPaso(int mPaso);
}
