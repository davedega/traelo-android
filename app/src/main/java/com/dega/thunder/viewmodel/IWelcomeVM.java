package com.dega.thunder.viewmodel;

/**
 * Created by davedega on 29/01/15.
 */
public interface IWelcomeVM {

    public void setListener(IWelcomeVMListener listener);

    public void hideButtonsSection();

    public void hideLoginSection();

    public void hideSignUpSection();

    public void showLoginSection();

    public void showSignUpSection();

    //Seccion Log In
    public String getLoginUsername();

    public String getLoginPassword();

    //Seccion Sign Up

    public String getSignUpUsername();

    public String getSignUpEmail();

    public String getSignUpPassword();


}
