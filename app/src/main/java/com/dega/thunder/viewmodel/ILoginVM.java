package com.dega.thunder.viewmodel;

/**
 * Created by davedega on 30/01/15.
 */
public interface ILoginVM {

    public String getLoginEmail();

    public String getLoginPassword();
}
