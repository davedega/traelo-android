package com.dega.thunder.viewmodel;

/**
 * Created by davedega on 29/01/15.
 */
public interface IRegistroVm {


    public String getSignUpEmail();

    public String getSignUpPassword();

}
