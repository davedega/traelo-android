package com.dega.thunder.rest;

import android.util.Log;

import com.dega.thunder.model.PlaceAutoComplete;
import com.google.android.gms.maps.model.LatLng;
import com.google.common.io.LineReader;
import com.google.gson.JsonObject;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicHeader;
import org.apache.http.protocol.HTTP;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;

/**
 * Created by davedega on 07/01/15.
 */
public class WebService {

    private static final String LOG_TAG = "ExampleApp";

    private static final String GEOCODE_API_BASE = "https://maps.googleapis.com/maps/api/geocode";
    private static final String PLACES_API_BASE = "https://maps.googleapis.com/maps/api/place";
    private static final String TYPE_AUTOCOMPLETE = "/autocomplete";
    private static final String TYPE_DETAIL = "/details";


    private static final String OUT_JSON = "/json";

    private static final String API_KEY = "AIzaSyCH20ykdPCKnDR1zY8munifQwwvGzNyNyA";

    public static ArrayList<String> autocomplete(String input, LatLng location) {
        ArrayList<String> resultList = null;

        HttpURLConnection conn = null;
        StringBuilder jsonResults = new StringBuilder();
        try {


            StringBuilder sb = new StringBuilder(PLACES_API_BASE + TYPE_AUTOCOMPLETE + OUT_JSON);
            sb.append("?key=" + API_KEY);
            if (input != null) {
                sb.append("&input=" + URLEncoder.encode(input, "utf8"));
                sb.append("&components=country:mx");
            } else {
                sb.append("&location=" + location.latitude + "," + location.longitude);
            }

            Log.i("URL: ", "" + sb.toString());
            URL url = new URL(sb.toString());
            conn = (HttpURLConnection) url.openConnection();
            InputStreamReader in = new InputStreamReader(conn.getInputStream());

            // Load the results into a StringBuilder
            int read;
            char[] buff = new char[1024];
            while ((read = in.read(buff)) != -1) {
                jsonResults.append(buff, 0, read);
            }
        } catch (MalformedURLException e) {
            Log.e(LOG_TAG, "Error processing Places API URL", e);
            return resultList;
        } catch (IOException e) {
            Log.e(LOG_TAG, "Error connecting to Places API", e);
            return resultList;
        } finally {
            if (conn != null) {
                conn.disconnect();
            }
        }

        try {
            // Create a JSON object hierarchy from the results
            JSONObject jsonObj = new JSONObject(jsonResults.toString());
            JSONArray predsJsonArray = jsonObj.getJSONArray("predictions");

            // Extract the Place descriptions from the results
            resultList = new ArrayList<String>(predsJsonArray.length());
            for (int i = 0; i < predsJsonArray.length(); i++) {
                resultList.add(predsJsonArray.getJSONObject(i).getString("description"));
            }
        } catch (JSONException e) {
            Log.e(LOG_TAG, "Cannot process JSON results", e);
        }

        return resultList;
    }


    public static ArrayList<String> getNearPlace(LatLng location) {
        ArrayList<String> resultList = null;

        HttpURLConnection conn = null;
        StringBuilder jsonResults = new StringBuilder();
        try {


            StringBuilder sb = new StringBuilder(GEOCODE_API_BASE  + OUT_JSON);
//            sb.append("?key=" + API_KEY);

            sb.append("?latlng=" + location.latitude + "," + location.longitude);


            Log.i("URL: ", "" + sb.toString());
            URL url = new URL(sb.toString());
            conn = (HttpURLConnection) url.openConnection();
            InputStreamReader in = new InputStreamReader(conn.getInputStream());

            // Load the results into a StringBuilder
            int read;
            char[] buff = new char[1024];
            while ((read = in.read(buff)) != -1) {
                jsonResults.append(buff, 0, read);
            }
        } catch (MalformedURLException e) {
            Log.e(LOG_TAG, "Error processing Places API URL", e);
            return resultList;
        } catch (IOException e) {
            Log.e(LOG_TAG, "Error connecting to Places API", e);
            return resultList;
        } finally {
            if (conn != null) {
                conn.disconnect();
            }
        }

        try {
            // Create a JSON object hierarchy from the results
            JSONObject jsonObj = new JSONObject(jsonResults.toString());
            JSONArray predsJsonArray = jsonObj.getJSONArray("results");

            // Extract the Place descriptions from the results
            resultList = new ArrayList<String>(predsJsonArray.length());
            for (int i = 0; i < predsJsonArray.length(); i++) {
                resultList.add(predsJsonArray.getJSONObject(i).getString("formatted_address"));
            }
        } catch (JSONException e) {
            Log.e(LOG_TAG, "Cannot process JSON results", e);
        }

        return resultList;
    }


    public static ArrayList<PlaceAutoComplete> getAddresses(String input) {
        ArrayList<PlaceAutoComplete> resultList = null;

        HttpURLConnection conn = null;
        StringBuilder jsonResults = new StringBuilder();
        try {
            StringBuilder sb = new StringBuilder(PLACES_API_BASE + TYPE_AUTOCOMPLETE + OUT_JSON);
            sb.append("?key=" + API_KEY);
            sb.append("&components=country:mx");
            sb.append("&input=" + URLEncoder.encode(input, "utf8"));
            Log.i("URL: ", "" + sb.toString());
            URL url = new URL(sb.toString());
            conn = (HttpURLConnection) url.openConnection();
            InputStreamReader in = new InputStreamReader(conn.getInputStream());

            // Load the results into a StringBuilder
            int read;
            char[] buff = new char[1024];
            while ((read = in.read(buff)) != -1) {
                jsonResults.append(buff, 0, read);
            }
        } catch (MalformedURLException e) {
            Log.e(LOG_TAG, "Error processing Places API URL", e);
            return resultList;
        } catch (IOException e) {
            Log.e(LOG_TAG, "Error connecting to Places API", e);
            return resultList;
        } finally {
            if (conn != null) {
                conn.disconnect();
            }
        }

        try {
            // Create a JSON object hierarchy from the results
            JSONObject jsonObj = new JSONObject(jsonResults.toString());
            JSONArray predsJsonArray = jsonObj.getJSONArray("predictions");

            // Extract the Place descriptions from the results
            resultList = new ArrayList<PlaceAutoComplete>(predsJsonArray.length());
            for (int i = 0; i < predsJsonArray.length(); i++) {
                PlaceAutoComplete place = new PlaceAutoComplete();
                place.description = predsJsonArray.getJSONObject(i).getString("description");
                place.place_id = predsJsonArray.getJSONObject(i).getString("place_id");
                resultList.add(place);
            }
        } catch (JSONException e) {
            Log.e(LOG_TAG, "Cannot process JSON results", e);
        }

        return resultList;
    }


    public static String doGet(String url) {
        String strResponse = null;

        HttpClient httpClient = new DefaultHttpClient();
        HttpGet httpGet = new HttpGet(url);
        httpGet.setHeader("Content-type", "application/json");

        Log.i("doGet", "url > " + url);

        try {
            HttpEntity httpEntity;

            HttpResponse response = httpClient.execute(httpGet);
            LineReader reader = new LineReader(new InputStreamReader(response
                    .getEntity().getContent()));

            strResponse = reader.toString();
            Log.i("doGet", "out >" + strResponse);


        } catch (ClientProtocolException e) {
            throw new RuntimeException("Error http", e);
        } catch (IOException e) {
            Log.i("IOException", "" + e);
            //throw new InternetConnectionException(e.toString(), e);
        }
        return strResponse;
    }


    public static String doPost(JsonObject jsonObject, String url) {
        String strResponse = null;

        HttpClient httpClient = new DefaultHttpClient();
        HttpPost httpPost = new HttpPost(url);
        httpPost.setHeader("Content-type", "application/json");

        Log.i("doPost", "url > " + url);
        Log.i("doPost", "in > " + jsonObject.toString());

        try {
            HttpEntity httpEntity;
            StringEntity stringEntity = new StringEntity(jsonObject.toString());
            stringEntity.setContentEncoding(new BasicHeader(HTTP.CONTENT_TYPE,
                    "application/json"));
            httpEntity = stringEntity;
            httpPost.setEntity(httpEntity);
            HttpResponse response = httpClient.execute(httpPost);
            LineReader reader = new LineReader(new InputStreamReader(response
                    .getEntity().getContent()));

            strResponse = reader.readLine();
            Log.i("doPost", "out >" + response);
            if (strResponse.contains("<!DOCTYPE")) {
                //  throw new InvalidUserException("Invalid user");
            }

        } catch (ClientProtocolException e) {
            throw new RuntimeException("Error http", e);
        } catch (IOException e) {
            Log.i("IOException", "" + e);
            //throw new InternetConnectionException(e.toString(), e);
        }
        return strResponse;
    }


    public static LatLng getAddressLocation(String place_id) {
        LatLng resultLatLng = null;
        HttpURLConnection conn = null;
        StringBuilder jsonResults = new StringBuilder();
        try {
            StringBuilder sb = new StringBuilder(PLACES_API_BASE + TYPE_DETAIL + OUT_JSON);
            sb.append("?key=" + API_KEY);
            sb.append("&placeid=" + place_id);
            Log.i("URL: ", "" + sb.toString());
            URL url = new URL(sb.toString());
            conn = (HttpURLConnection) url.openConnection();
            InputStreamReader in = new InputStreamReader(conn.getInputStream());

            // Load the results into a StringBuilder
            int read;
            char[] buff = new char[1024];
            while ((read = in.read(buff)) != -1) {
                jsonResults.append(buff, 0, read);
            }
        } catch (MalformedURLException e) {
            Log.e(LOG_TAG, "Error processing Places API URL", e);
            return null;
        } catch (IOException e) {
            Log.e(LOG_TAG, "Error connecting to Places API", e);
            return null;
        } finally {
            if (conn != null) {
                conn.disconnect();
            }
        }

        try {
            // Create a JSON object hierarchy from the results
            JSONObject jsonObj = new JSONObject(jsonResults.toString());
            JSONObject location = jsonObj.getJSONObject("result").getJSONObject("geometry").getJSONObject("location");
            resultLatLng = new LatLng(location.getDouble("lat"), location.getDouble("lng"));

        } catch (JSONException e) {
            Log.e(LOG_TAG, "Cannot process JSON results", e);
        }

        return resultLatLng;
    }
}
