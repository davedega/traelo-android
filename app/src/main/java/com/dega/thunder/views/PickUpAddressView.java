package com.dega.thunder.views;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.dega.thunder.R;
import com.dega.thunder.model.Constants;
import com.dega.thunder.viewmodel.IPickUpAddressVM;
import com.dega.thunder.viewmodel.IPickUpAddressVMListener;

import roboguice.fragment.RoboFragment;
import roboguice.inject.InjectView;


public class PickUpAddressView extends RoboFragment implements IPickUpAddressVM {


    IPickUpAddressVMListener mListener;

    @InjectView(R.id.seccion_tipo_servicio)
    LinearLayout seccionTipoServicio;
    @InjectView(R.id.seccion_forma_de_pago)
    LinearLayout seccionFormaDePago;

    @InjectView(R.id.texto_accion)
    TextView textoTipoAccion;
    @InjectView(R.id.calle_et)
    EditText mCalle;
    @InjectView(R.id.numero_et)
    EditText mNumero;
    @InjectView(R.id.referencia_et)
    EditText mReferencia;
    @InjectView(R.id.descripcion_paquete_et)
    EditText mPaquete;
    @InjectView(R.id.nombre_o_comercio_et)
    EditText nNombreOComercio;
    @InjectView(R.id.tipo_servicio_rg)
    RadioGroup tipoServicio;
    @InjectView(R.id.next_btn)
    Button nextBtn;


    public PickUpAddressView() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        LinearLayout mView = (LinearLayout) inflater.inflate(R.layout.fragment_direccion, container, false);

        return mView;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        nextBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (validaCampos()) {
                    mListener.onNextT();
                }
            }
        });
    }

    @Override
    public void setListener(IPickUpAddressVMListener listener) {
        mListener = listener;
    }

    @Override
    public void setStreet(String street) {
        mCalle.setText(street);
    }

    @Override
    public String getCalle() {
        return mCalle.getText().toString();
    }

    @Override
    public String getNumero() {
        return mNumero.getText().toString();
    }

    @Override
    public String getReferencia() {
        return mReferencia.getText().toString();
    }

    @Override
    public String getNombrePersonaOComercio() {
        return nNombreOComercio.getText().toString();
    }

    @Override
    public String getDescripcionPaquete() {
        return mPaquete.getText().toString();
    }

    @Override
    public boolean getTipoServicio() {
        return false;
    }

    @Override
    public void setViewPaso(int mPaso) {
        switch (mPaso) {

            case Constants.PASO_UNO:
                seccionTipoServicio.setVisibility(View.VISIBLE);
                seccionFormaDePago.setVisibility(View.INVISIBLE);
                textoTipoAccion.setText(getString(R.string.donde_recojer));
                nextBtn.setText(getString(R.string.enviar_a));
                break;

            case Constants.PASO_DOS:
                seccionTipoServicio.setVisibility(View.INVISIBLE);
                seccionFormaDePago.setVisibility(View.VISIBLE);
                textoTipoAccion.setText(getString(R.string.donde_entregar));
                nextBtn.setText(getString(R.string.listo));
                break;

            default:
                break;
        }
    }

    public boolean validaCampos() {
        boolean ok = true;
        clearError();
        if (mCalle.getText().toString().equals("")) {
            mCalle.setError(getString(R.string.must_fill));
            ok = false;
        }
        if (mNumero.getText().toString().equals("")) {
            mNumero.setError(getString(R.string.must_fill));
            ok = false;
        }
        if (mReferencia.getText().toString().equals("")) {
            mReferencia.setError(getString(R.string.must_fill));
            ok = false;
        }
        if (mPaquete.getText().toString().equals("")) {
            mPaquete.setError(getString(R.string.must_fill));
            ok = false;
        }
        if (nNombreOComercio.getText().toString().equals("")) {
            nNombreOComercio.setError(getString(R.string.must_fill));
            ok = false;
        }
        return ok;
    }

    public void clearError() {
        mCalle.setError(null);
        mNumero.setError(null);
        mReferencia.setError(null);
        mPaquete.setError(null);
        nNombreOComercio.setError(null);
    }
}
