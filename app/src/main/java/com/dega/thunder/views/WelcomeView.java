package com.dega.thunder.views;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.dega.thunder.R;
import com.dega.thunder.viewmodel.IWelcomeVM;
import com.dega.thunder.viewmodel.IWelcomeVMListener;

import roboguice.fragment.RoboFragment;
import roboguice.inject.InjectView;

public class WelcomeView extends RoboFragment implements IWelcomeVM, View.OnClickListener {
    IWelcomeVMListener mListener;

    //Welcome Buttons Section
    @InjectView(R.id.to_view_login)
    Button mViewLogin;
    @InjectView(R.id.to_view_signup)
    Button mViewLSignUp;


    //Login Section
    @InjectView(R.id.login_username)
    EditText mLoginUsername;
    @InjectView(R.id.login_password)
    EditText mLoginPassword;
    @InjectView(R.id.do_login)
    Button mDoLogin;
    @InjectView(R.id.signup)
    TextView mSignup;

    //SignUp Section
    @InjectView(R.id.signup_username)
    EditText mSignUpUsername;
    @InjectView(R.id.signup_email)
    EditText mSignUpEmail;
    @InjectView(R.id.signup_password)
    EditText mSignUpPassword;
    @InjectView(R.id.signup_verify_password)
    EditText mSignUpVerifyPassword;
    @InjectView(R.id.do_signup)
    Button mDoLSignUp;
    @InjectView(R.id.login)
    TextView mLogin;

    //Sections
    @InjectView(R.id.buttons_container)
    LinearLayout buttonsSection;
    @InjectView(R.id.login_container)
    LinearLayout logInSection;
    @InjectView(R.id.signup_container)
    LinearLayout signUpSection;

    int mShortAnimationDuration;

    public WelcomeView() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_welcome, container, false);
        return rootView;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mViewLogin.setOnClickListener(this);
        mViewLSignUp.setOnClickListener(this);
        mDoLogin.setOnClickListener(this);
        mSignup.setOnClickListener(this);
        mDoLSignUp.setOnClickListener(this);
        mLogin.setOnClickListener(this);
        //oculto todas las vistas complementarias
        logInSection.setVisibility(View.GONE);
        signUpSection.setVisibility(View.GONE);

    }

    @Override
    public void setListener(IWelcomeVMListener listener) {
        mListener = listener;
    }


    @Override
    public String getSignUpEmail() {
        return mSignUpEmail.getText().toString();
    }

    @Override
    public String getSignUpPassword() {
        return mSignUpPassword.getText().toString();
    }


    @Override
    public String getLoginUsername() {
        return mLoginUsername.getText().toString();
    }

    @Override
    public String getLoginPassword() {
        return mLoginPassword.getText().toString();
    }

    @Override
    public String getSignUpUsername() {
        return mSignUpUsername.getText().toString();
    }

    @Override
    public void hideButtonsSection() {
        mShortAnimationDuration = getResources().getInteger(
                android.R.integer.config_shortAnimTime);

        // Animate the loading view to 0% opacity. After the animation ends,
        // set its visibility to GONE as an optimization step (it won't
        // participate in layout passes, etc.)
        buttonsSection.animate()
                .alpha(0f)
                .setDuration(mShortAnimationDuration)
                .setListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        buttonsSection.setVisibility(View.GONE);
                    }
                });
    }

    @Override
    public void hideLoginSection() {
        mShortAnimationDuration = getResources().getInteger(
                android.R.integer.config_shortAnimTime);

        // Animate the loading view to 0% opacity. After the animation ends,
        // set its visibility to GONE as an optimization step (it won't
        // participate in layout passes, etc.)
        logInSection.animate()
                .alpha(0f)
                .setDuration(mShortAnimationDuration)
                .setListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        logInSection.setVisibility(View.GONE);
                    }
                });
    }

    @Override
    public void hideSignUpSection() {
        mShortAnimationDuration = getResources().getInteger(
                android.R.integer.config_shortAnimTime);

        // Animate the loading view to 0% opacity. After the animation ends,
        // set its visibility to GONE as an optimization step (it won't
        // participate in layout passes, etc.)
        signUpSection.animate()
                .alpha(0f)
                .setDuration(mShortAnimationDuration)
                .setListener(new AnimatorListenerAdapter() {
                    @Override
                    public void onAnimationEnd(Animator animation) {
                        signUpSection.setVisibility(View.GONE);
                    }
                });
    }

    @Override
    public void showLoginSection() {
        hideButtonsSection();
        hideSignUpSection();


        // Set the content view to 0% opacity but visible, so that it is visible
        // (but fully transparent) during the animation.
        logInSection.setAlpha(0f);
        logInSection.setVisibility(View.VISIBLE);

        // Animate the content view to 100% opacity, and clear any animation
        // listener set on the view.
        logInSection.animate()
                .alpha(1f)
                .setDuration(mShortAnimationDuration)
                .setListener(null);

    }

    @Override
    public void showSignUpSection() {
        hideButtonsSection();
        hideLoginSection();
        // Set the content view to 0% opacity but visible, so that it is visible
        // (but fully transparent) during the animation.
        signUpSection.setAlpha(0f);
        signUpSection.setVisibility(View.VISIBLE);

        // Animate the content view to 100% opacity, and clear any animation
        // listener set on the view.
        signUpSection.animate()
                .alpha(1f)
                .setDuration(mShortAnimationDuration)
                .setListener(null);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.to_view_login:
                mListener.onViewLoginT();
                break;

            case R.id.to_view_signup:
                mListener.onViewSignUpT();
                break;

            case R.id.do_login:
                mListener.onDoLoginT();
                break;
            case R.id.do_signup:
                mListener.onDoSignUpT();
                break;

            case R.id.signup:
                mListener.onViewSignUpT();
                break;
            case R.id.login:
                mListener.onViewLoginT();
                break;


            default:
                break;
        }
    }

}