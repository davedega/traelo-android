package com.dega.thunder.views;

import android.app.Activity;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.dega.thunder.MainActivity;
import com.dega.thunder.R;
import com.dega.thunder.Utils.AppPreferences;
import com.dega.thunder.controller.LocationActivity;
import com.dega.thunder.model.Constants;

import roboguice.fragment.RoboFragment;
import roboguice.inject.InjectView;

/**
 * Created by davedega on 01/02/15.
 */
public class HomeView extends RoboFragment implements View.OnClickListener{

    private static final String ARG_SECTION_NUMBER = "section_number";

    // View Crear Pedido
    @InjectView(R.id.create_request_container)
    LinearLayout mCreateSectionn;
    @InjectView(R.id.pide_heyboy)
    Button mPideHeyBoy;

    // View Continuar Pedido
    @InjectView(R.id.continue_request_container)
    LinearLayout mContinueSection;
    @InjectView(R.id.origen_paquete)
    TextView mOrigen;
    @InjectView(R.id.descripcion_paquete)
    TextView mPaquete;
    @InjectView(R.id.destino_paquete)
    TextView mDestino;
    @InjectView(R.id.forma_de_pago)
    TextView mFormaDePago;
    @InjectView(R.id.continuar_pedido)
    Button mContinuarPedido;


    public static HomeView newInstance(int param1) {
        HomeView fragment = new HomeView();
        Bundle args = new Bundle();
        args.putInt(ARG_SECTION_NUMBER, param1);
        fragment.setArguments(args);
        return fragment;
    }

    public HomeView() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_home_view, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        Log.i("HOME FRAGMENT", "onViewCreated()");
        mPideHeyBoy.setOnClickListener(this);
        if (AppPreferences.isStartedRequest()) {
            showContinueRequestView();
        } else {
            showCreateRequestView();
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        ((MainActivity) activity).onSectionAttached(
                getArguments().getInt(ARG_SECTION_NUMBER));

    }

    public void showCreateRequestView() {
        mCreateSectionn.setVisibility(View.VISIBLE);
        mContinueSection.setVisibility(View.GONE);
    }

    public void showContinueRequestView() {
        mContinueSection.setVisibility(View.VISIBLE);
        mCreateSectionn.setVisibility(View.GONE);
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.pide_heyboy:
                LocationActivity.start(getActivity(),Constants.PASO_UNO);
                break;
            default:
                break;
        }
    }
}
