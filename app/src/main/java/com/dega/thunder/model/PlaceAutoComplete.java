package com.dega.thunder.model;

/**
 * Created by davedega on 07/01/15.
 */
public class PlaceAutoComplete {

    public String place_id;
    public String description;

    public PlaceAutoComplete() {
    }

    @Override
    public String toString() {
        return description;
    }
}
