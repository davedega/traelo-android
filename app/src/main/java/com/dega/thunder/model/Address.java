package com.dega.thunder.model;

/**
 * Created by davedega on 02/02/15.
 */
public class Address {
    public String street;
    public String number;
    public String reference;
    public String namePerson;
}
