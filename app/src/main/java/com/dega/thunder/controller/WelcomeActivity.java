package com.dega.thunder.controller;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import com.dega.thunder.MainActivity;
import com.dega.thunder.R;
import com.dega.thunder.ui.WaitDialog;
import com.dega.thunder.viewmodel.IWelcomeVM;
import com.dega.thunder.viewmodel.IWelcomeVMListener;
import com.parse.LogInCallback;
import com.parse.ParseException;
import com.parse.ParseUser;
import com.parse.SignUpCallback;

import roboguice.activity.RoboActionBarActivity;
import roboguice.inject.ContentView;


@ContentView(R.layout.activity_welcome)
public class WelcomeActivity extends RoboActionBarActivity implements IWelcomeVMListener {

    IWelcomeVM viewModel;
    WaitDialog taxyDialog;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        viewModel = (IWelcomeVM) getSupportFragmentManager().findFragmentById(R.id.fragment_welcome);
        viewModel.setListener(this);
        taxyDialog = WaitDialog.newInstance();
//		taxyDialog.setCanceledOnTouchOutside(false);
        taxyDialog.setCancelable(false);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_welcome, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }


    @Override
    public void onViewLoginT() {
        viewModel.showLoginSection();
    }

    @Override
    public void onViewSignUpT() {
        viewModel.showSignUpSection();
    }

    @Override
    public void onDoLoginT() {

        taxyDialog.show(getSupportFragmentManager(), "dialog");

        ParseUser.logInInBackground(viewModel.getLoginUsername(), viewModel.getLoginPassword(), new LogInCallback() {
            @Override
            public void done(ParseUser parseUser, ParseException e) {
                taxyDialog.dismiss();
                if (e != null) {
                    Log.i("DONE", "con error :( " + e.getMessage());
                    Toast.makeText(getApplicationContext(),
                            "Sign up Error"+e.getMessage(), Toast.LENGTH_LONG)
                            .show();
                } else {
                    Log.i("DONE", "sin error ");
                    startActivity(new Intent(getApplicationContext(), MainActivity.class));
                    finish();
                }
            }
        });

    }

    @Override
    public void onDoSignUpT() {
        taxyDialog.show(getSupportFragmentManager(), "dialog");

        // Save new user data into Parse.com Data Storage
        ParseUser user = new ParseUser();
        user.setEmail(viewModel.getSignUpEmail());
        user.setUsername(viewModel.getSignUpUsername());
        user.setPassword(viewModel.getSignUpPassword());
        user.signUpInBackground(new SignUpCallback() {
            public void done(ParseException e) {
                taxyDialog.dismiss();

                if (e == null) {
                    // Show a simple Toast message upon successful registration
                    Toast.makeText(getApplicationContext(), getString(R.string.successfully), Toast.LENGTH_SHORT);
                    startActivity(new Intent(getApplicationContext(), MainActivity.class));
                    finish();
                } else {
                    Toast.makeText(getApplicationContext(),
                            "Sign up Error", Toast.LENGTH_LONG)
                            .show();
                }
            }
        });
    }
}
