package com.dega.thunder.controller;

import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.Toast;

import com.dega.thunder.R;
import com.dega.thunder.app.BaseActivity;
import com.dega.thunder.model.PlaceAutoComplete;
import com.dega.thunder.rest.WebService;
import com.dega.thunder.ui.ClearableAutoComplete;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks;
import com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;

import java.util.ArrayList;

import roboguice.inject.ContentView;


@ContentView(R.layout.activity_location)
public class LocationActivity extends BaseActivity implements ConnectionCallbacks,
        OnConnectionFailedListener {


    static GoogleMap mMap;
    protected GoogleApiClient mGoogleApiClient;
    protected Location mLastLocation;

    LatLng addressLocation;
    Button pedirGoukan;
    ClearableAutoComplete autoCompView;
    ArrayList<String> nearPlaces;
    ImageView getThisAddressBtn;
    int mPaso;

    public static void start(Context ctx, int paso) {
        Intent intent = new Intent(ctx, LocationActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.putExtra("paso", paso);
        ctx.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mPaso = getIntent().getExtras().getInt("paso");

        autoCompView = (ClearableAutoComplete) findViewById(R.id.from);
        autoCompView.setAdapter(new PlacesAutoCompleteAdapter(this, R.layout.list_item));
        autoCompView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int position, long id) {
                PlaceAutoComplete str = (PlaceAutoComplete) adapterView.getItemAtPosition(position);
                Toast.makeText(getApplicationContext(), str.place_id, Toast.LENGTH_SHORT).show();
                hideKeyboard(getApplicationContext());
                getAddressLocation(str.place_id);
            }


        });
        pedirGoukan = (Button) findViewById(R.id.pedir_goukan);
        pedirGoukan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                autoCompView.setError(null);
                if (autoCompView.getText().toString().equals("")) {
                    autoCompView.setError(getString(R.string.dinos_donde_recojerlo));
                } else {
                    PickUpAddressActivity.start(getApplicationContext(), autoCompView.getText().toString(), mPaso);
                }
            }
        });
        getThisAddressBtn = (ImageView) findViewById(R.id.get_this_addreess_btn);
        getThisAddressBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                LatLng cameraLatLng = mMap.getCameraPosition().target;
                Log.i("    onCameraChange", "latitude: " + cameraLatLng.latitude);
                Log.i("    onCameraChange", "longitude: " + cameraLatLng.longitude);

                searchNearAddresss(cameraLatLng);

            }
        });
        setUpMapIfNeeded();
        buildGoogleApiClient();


    }

    /**
     * Builds a GoogleApiClient. Uses the addApi() method to request the LocationServices API.
     */
    protected synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
    }

    @Override
    protected void onStart() {
        super.onStart();
        mGoogleApiClient.connect();
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (mGoogleApiClient.isConnected()) {
            mGoogleApiClient.disconnect();
        }
    }

    private void getAddressLocation(final String place_id) {
        showWaitDialogWhileExecuting("", new Runnable() {
            @Override
            public void run() {
                addressLocation = WebService.getAddressLocation(place_id);
            }
        }, new Runnable() {
            @Override
            public void run() {
                zoomAt(addressLocation);
            }
        });
    }

    private void zoomAt(LatLng addressLocation) {
        if (addressLocation != null) {
            Log.i("FINAL", "latitude: " + addressLocation.latitude);
            Log.i("FINAL", "longitude: " + addressLocation.longitude);
//            mMap.addMarker(new MarkerOptions().position(addressLocation).title(getString(R.string.en_esta_calle)));
            mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(addressLocation, 15));
        }

    }


    @Override
    protected void onResume() {
        super.onResume();
        setUpMapIfNeeded();
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_direccion_controller, menu);
        return true;
    }

    /**
     * Sets up the map if it is possible to do so (i.e., the Google Play services APK is correctly
     * installed) and the map has not already been instantiated.. This will ensure that we only ever
     * call {@link #setUpMap()} once when {@link #mMap} is not null.
     * <p/>
     * If it isn't installed {@link com.google.android.gms.maps.SupportMapFragment} (and
     * {@link com.google.android.gms.maps.MapView MapView}) will show a prompt for the user to
     * install/update the Google Play services APK on their device.
     * <p/>
     * A user can return to this FragmentActivity after following the prompt and correctly
     * installing/updating/enabling the Google Play services. Since the FragmentActivity may not
     * have been completely destroyed during this process (it is likely that it would only be
     * stopped or paused), {@link #onCreate(android.os.Bundle)} may not be called again so we should call this
     * method in {@link #onResume()} to guarantee that it will be called.
     */
    private void setUpMapIfNeeded() {
        // Do a null check to confirm that we have not already instantiated the map.
        if (mMap == null) {
            // Try to obtain the map from the SupportMapFragment.
            mMap = ((SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map_home))
                    .getMap();
            // Check if we were successful in obtaining the map.
            if (mMap != null) {
                setUpMap();
            }
        }
    }

    /**
     * This is where we can add markers or lines, add listeners or move the camera. In this case, we
     * just add a marker near Africa.
     * <p/>
     * This should only be called once and when we are sure that {@link #mMap} is not null.
     */
    private void setUpMap() {
        mMap.setMyLocationEnabled(true);
        mMap.setOnCameraChangeListener(new GoogleMap.OnCameraChangeListener() {
            @Override
            public void onCameraChange(CameraPosition cameraPosition) {

                final double cLatitude = mMap.getCameraPosition().target.latitude;
                final double cLongitud = mMap.getCameraPosition().target.longitude;

                executeInBackground(new Runnable() {
                    @Override
                    public void run() {
                        nearPlaces = WebService.getNearPlace(new LatLng(cLatitude,cLongitud));
                    }
                },new Runnable() {
                    @Override
                    public void run() {
                        if (nearPlaces != null)
                            autoCompView.setText(nearPlaces.get(0));
                    }
                });
            }
        });
    }

    private void searchNearAddresss(final LatLng cameraLatLng) {
        showWaitDialogWhileExecuting("", new Runnable() {
            @Override
            public void run() {
                nearPlaces = WebService.getNearPlace(cameraLatLng);
                Log.i("", "size: " + nearPlaces.size());
            }
        }, new Runnable() {
            @Override
            public void run() {
                if (nearPlaces != null)
                    autoCompView.setText(nearPlaces.get(0));
            }
        });

    }

    @Override
    public void onConnected(Bundle bundle) {
        // Provides a simple way of getting a device's location and is well suited for
        // applications that do not require a fine-grained location and that do not need location
        // updates. Gets the best and most recent location currently available, which may be null
        // in rare cases when a location is not available.
        mLastLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
        if (mLastLocation != null) {
            CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(new LatLng(mLastLocation.getLatitude(), mLastLocation.getLongitude()), 15);
            mMap.animateCamera(cameraUpdate);

        } else {
            Toast.makeText(this, R.string.no_location_detected, Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void onConnectionSuspended(int i) {
        mGoogleApiClient.connect();

    }


    @Override
    public void onConnectionFailed(ConnectionResult result) {
        Log.i("LocationActivity", "Connection failed: ConnectionResult.getErrorCode() = " + result.getErrorCode());

    }

    private class PlacesAutoCompleteAdapter extends ArrayAdapter<PlaceAutoComplete> implements Filterable {
        private ArrayList<PlaceAutoComplete> resultList;

        public PlacesAutoCompleteAdapter(Context context, int textViewResourceId) {
            super(context, textViewResourceId);
        }

        @Override
        public int getCount() {
            return resultList.size();
        }

        @Override
        public PlaceAutoComplete getItem(int index) {
            return resultList.get(index);
        }


        @Override
        public Filter getFilter() {
            Filter filter = new Filter() {
                @Override
                protected FilterResults performFiltering(CharSequence constraint) {
                    FilterResults filterResults = new FilterResults();
                    if (constraint != null) {
                        // Retrieve the autocomplete results.
                        resultList = WebService.getAddresses(constraint.toString());

                        // Assign the data to the FilterResults
                        filterResults.values = resultList;
                        filterResults.count = resultList.size();
                    }
                    return filterResults;
                }

                @Override
                protected void publishResults(CharSequence constraint, FilterResults results) {
                    if (results != null && results.count > 0) {
                        notifyDataSetChanged();
                    } else {
                        notifyDataSetInvalidated();
                    }
                }
            };
            return filter;
        }
    }
}
