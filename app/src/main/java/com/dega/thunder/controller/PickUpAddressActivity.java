package com.dega.thunder.controller;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

import com.dega.thunder.R;
import com.dega.thunder.app.BaseActivity;
import com.dega.thunder.model.Constants;
import com.dega.thunder.model.PickUpAddress;
import com.dega.thunder.viewmodel.IPickUpAddressVM;
import com.dega.thunder.viewmodel.IPickUpAddressVMListener;

public class PickUpAddressActivity extends BaseActivity implements IPickUpAddressVMListener {

    IPickUpAddressVM viewModel;
    int mPaso;

    public static void start(Context ctx, String street, int paso) {
        Intent intent = new Intent(ctx, PickUpAddressActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.putExtra("street", street);
        intent.putExtra("paso", paso);
        ctx.startActivity(intent);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.i("Direccion Conrroller", "onCreate");
        setContentView(R.layout.activity_direccion_controller);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);


        String mStreet = getIntent().getExtras().getString("street");
        mPaso = getIntent().getExtras().getInt("paso");

        viewModel = (IPickUpAddressVM) getSupportFragmentManager().findFragmentById(R.id.fragment_direc);
        viewModel.setListener(this);
        viewModel.setStreet(mStreet);
        viewModel.setViewPaso(mPaso);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_direccion_controller, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                break;

            default:
                break;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onNextT() {
        PickUpAddress address = new PickUpAddress();
        address.street = viewModel.getCalle();
        address.number = viewModel.getNumero();
        address.reference = viewModel.getReferencia();
        address.namePerson = viewModel.getNombrePersonaOComercio();
        address.packageToDelivery = viewModel.getDescripcionPaquete();

        LocationActivity.start(getApplicationContext(), Constants.PASO_DOS);

    }
}
