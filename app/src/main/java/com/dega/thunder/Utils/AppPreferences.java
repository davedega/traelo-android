package com.dega.thunder.Utils;

import android.content.Context;
import android.content.SharedPreferences;

import com.dega.thunder.app.THUNDER;
import com.dega.thunder.model.Address;
import com.dega.thunder.model.PickUpAddress;

/**
 * Created by davedega on 02/02/15.
 */
public class AppPreferences {


    public static boolean isStartedRequest() {
        SharedPreferences settings = THUNDER.getAppContext().getSharedPreferences(
                "request", Context.MODE_PRIVATE);
        return settings.getBoolean("request", false);
    }

    public static void setStartedRequest() {
        SharedPreferences.Editor editor = THUNDER.getAppContext()
                .getSharedPreferences("request", Context.MODE_PRIVATE).edit();
        editor.putBoolean("started", true);
        editor.commit();
    }

    public static void setFinishedRequest() {
        SharedPreferences.Editor editor = THUNDER.getAppContext()
                .getSharedPreferences("request", Context.MODE_PRIVATE).edit();
        editor.putBoolean("started", false);
        editor.commit();
    }

    public static void setPickUpAddress(PickUpAddress address) {
        SharedPreferences.Editor editor = THUNDER.getAppContext()
                .getSharedPreferences("pickUpAddress", Context.MODE_PRIVATE).edit();
        editor.putString("street", address.street);
        editor.putString("number", address.number);
        editor.putString("reference", address.reference);
        editor.putString("namePerson", address.namePerson);
        editor.putString("packageToDelivery", address.packageToDelivery);
        editor.commit();
    }

    public static PickUpAddress getPickUpAddress() {
        PickUpAddress pickUpAddress;
        SharedPreferences settings = THUNDER.getAppContext().getSharedPreferences(
                "pickUpAddress", Context.MODE_PRIVATE);
        pickUpAddress = new PickUpAddress();
        pickUpAddress.street = settings.getString("street", "");
        pickUpAddress.number = settings.getString("number", "");
        pickUpAddress.reference = settings.getString("reference", "");
        pickUpAddress.namePerson = settings.getString("namePerson", "");
        pickUpAddress.packageToDelivery = settings.getString("packageToDelivery", "");
        return pickUpAddress;
    }


    public static void setDeliverypAddress(Address address) {
        SharedPreferences.Editor editor = THUNDER.getAppContext()
                .getSharedPreferences("deliveryAddress", Context.MODE_PRIVATE).edit();
        editor.putString("street", address.street);
        editor.putString("number", address.number);
        editor.putString("reference", address.reference);
        editor.putString("namePerson", address.namePerson);
        editor.commit();
    }

    public static Address getDeliverypAddress() {
        Address deliveryAddress;
        SharedPreferences settings = THUNDER.getAppContext().getSharedPreferences(
                "deliveryAddress", Context.MODE_PRIVATE);
        deliveryAddress = new Address();
        deliveryAddress.street = settings.getString("street", "");
        deliveryAddress.number = settings.getString("number", "");
        deliveryAddress.reference = settings.getString("reference", "");
        deliveryAddress.namePerson = settings.getString("namePerson", "");
        return deliveryAddress;
    }

}
