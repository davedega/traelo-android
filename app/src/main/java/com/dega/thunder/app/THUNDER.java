package com.dega.thunder.app;

import android.app.Application;
import android.content.Context;

import com.dega.thunder.R;
import com.parse.Parse;

import roboguice.RoboGuice;

/**
 * Created by davedega on 29/01/15.
 */
public class THUNDER extends Application {

    private static Context _ctx;

    @Override
    public void onCreate() {
        super.onCreate();
        _ctx = getApplicationContext();
        RoboGuice.setUseAnnotationDatabases(false);
        Parse.initialize(this, getString(R.string.parse_app_id), getString(R.string.parse_client_key));
    }

    public static Context getAppContext() {
        return _ctx;
    }
}
