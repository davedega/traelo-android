package com.dega.thunder.app;

import android.content.Intent;
import android.os.Bundle;

import com.dega.thunder.MainActivity;
import com.dega.thunder.controller.WelcomeActivity;
import com.parse.ParseUser;

import roboguice.activity.RoboActionBarActivity;

public class DispatchActivity extends RoboActionBarActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (ParseUser.getCurrentUser() != null) {
            // Start an intent for the logged in activity
            startActivity(new Intent(this, MainActivity.class));
            finish();
        } else {
            // Start and intent for the logged out activity
            startActivity(new Intent(this, WelcomeActivity.class));
            finish();
        }

    }
}
