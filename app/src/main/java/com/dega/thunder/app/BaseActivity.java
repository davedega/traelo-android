package com.dega.thunder.app;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

import roboguice.activity.RoboActionBarActivity;

/**
 * Created by davedega on 02/02/15.
 */
public class BaseActivity extends RoboActionBarActivity {
    static ProgressDialog dialogo;

    @Override
    public View onCreateView(View parent, String name, Context context, AttributeSet attrs) {
        return super.onCreateView(parent, name, context, attrs);
    }

    public void showWaitDialogWhileExecuting(String msg,final Runnable task, final Runnable completion){
        doInBackgroundWithMessage(msg, task, completion);
    }

    public void executeInBackground(final Runnable task, final Runnable completion){
        doInBackground(task, completion);
    }

    protected void doInBackgroundWithMessage(String msg,final Runnable task, final Runnable completion) {
        dialogo = new ProgressDialog(this);
        dialogo.setMessage(msg);
        dialogo.setCanceledOnTouchOutside(false);
        dialogo.setCancelable(false);

        AsyncTask<Void, Void, Throwable> asyncTask = new AsyncTask<Void, Void, Throwable>() {
            @Override
            protected void onPreExecute() {
                super.onPreExecute();
                dialogo.show();
            }

            @Override
            protected Throwable doInBackground(Void... params) {
                try {
                    if (task != null)
                        task.run();
                } catch (Exception e) {
                    dialogo.dismiss();
                    Log.e("doInBackground", "Excpetion" + e);
                    return e;
                }
                return null;
            }

            @Override
            protected void onPostExecute(Throwable result) {
                if (result != null) {
                    dialogo.dismiss();
                    handleException(getApplicationContext(), result);
                } else if (completion != null) {
                    dialogo.dismiss();
                    completion.run();

                } else
                    dialogo.dismiss();
            }
        };
        asyncTask.execute();
    }

    protected void doInBackground(final Runnable task, final Runnable completion) {

        AsyncTask<Void, Void, Throwable> asyncTask = new AsyncTask<Void, Void, Throwable>() {
            @Override
            protected void onPreExecute() {
                super.onPreExecute();
            }

            @Override
            protected Throwable doInBackground(Void... params) {
                try {
                    if (task != null)
                        task.run();
                } catch (Exception e) {
                    Log.e("doInBackground", "Excpetion" + e);
                    return e;
                }
                return null;
            }

            @Override
            protected void onPostExecute(Throwable result) {
                if (result != null) {
                    handleException(getApplicationContext(), result);
                } else if (completion != null) {
                    completion.run();
                }
            }
        };
        asyncTask.execute();
    }

    /**
     * Cuando se genera una excepcion se llama este metodo para manejarla
     */
    public void handleException(Context ctx, Throwable caught) {
        Log.e("handleException()", "causa: " + caught.getMessage().toString());

//        if(caught instanceof InvalidUserException){
//            onInvalidUser(caught.getMessage().toString());
//        }
//        if(caught instanceof NoLocationException){
//            onNoLocation(caught.getMessage());
//        }
    }

    public static void hideKeyboard(Context ctx){
        InputMethodManager imm = (InputMethodManager) ctx.getSystemService(Activity.INPUT_METHOD_SERVICE);
        imm.toggleSoftInput(InputMethodManager.HIDE_IMPLICIT_ONLY, 0);

    }
}
